import os
from os import system as runcmd
import subprocess
import PIL
from PIL import *
from PIL import Image,ImageTk
import webbrowser
import tkinter as tk
from tkinter import *
from tkinter import messagebox
from threading import *
currentUserName = os.getlogin()
#currentPath = os.path.dirname(os.path.abspath(__file__))
def startGame():
    runcmd("picomc play 1.12.2-forge-14.23.5.2854 --account " + userName.get())
def openSettings():
    os.startfile("settings.exe")
def openGame():
    runcmd("picomc account create " + userName.get())
    t1=Thread(target=startGame)
    t1.start()
root = tk.Tk()
root.title("LOLHUB")
root.geometry("923x500")
root.configure(background='#F4CD00')
root.iconbitmap("resources/favicon.ico")
userName = tk.StringVar()
render = ImageTk.PhotoImage(PIL.Image.open("resources/logo.png"))
logo = Label(root, image=render, bg="#F4CD00")
logo.pack()
usernameLabel = Label(root, text="Username:", font=("Roboto", 20), bg="#F4CD00", fg="#888787")
usernameLabel.pack()
usernameTextBox = Entry(root, textvariable=userName, bg="#888787")
usernameTextBox.pack(pady=10)
render2 = ImageTk.PhotoImage(PIL.Image.open("resources/playBTN.png"))
playBTN = Button(root, image=render2, bg="#F4CD00", cursor="hand2", command=openGame, activebackground="#F4CD00")
playBTN.pack()
settingsBTN = Button(root, command=openSettings, bg="#F4CD00", text="⚙️", cursor="hand2", activebackground="#F4CD00")
settingsBTN.place(x=900, y=0)
root.mainloop()
