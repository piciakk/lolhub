import os
from os import system as runcmd
import subprocess
import PIL
from PIL import *
from PIL import Image,ImageTk
import webbrowser
import tkinter as tk
from tkinter import filedialog
from tkinter import *
root = tk.Tk()
root.title("Settings")
root.geometry("923x500")
root.configure(background='#F4CD00')
root.iconbitmap("resources/favicon.ico")
#setting togglers
shaderFile = open("settings/shader", "r")
voiceChangerFile = open("settings/voiceChanger", "r")
if "0" in shaderFile.read():
    render2 = ImageTk.PhotoImage(PIL.Image.open("resources/inactive.png"))
else:
    render2 = ImageTk.PhotoImage(PIL.Image.open("resources/active.png"))
shaderFile.close()
if "0" in voiceChangerFile.read():
    render3 = ImageTk.PhotoImage(PIL.Image.open("resources/inactive.png"))
else:
    render3 = ImageTk.PhotoImage(PIL.Image.open("resources/active.png"))
voiceChangerFile.close()
#function for installing PicoMC
def installPico():
    runcmd("pip install picomc")
#function for toggling shaders
def toggleShader():
    shaderFile = open("settings/shader", "r")
    if "0" in shaderFile.read():
        shaderFile.close()
        shaderFile = open("settings/shader", "a")
        shaderFile.seek(0)
        shaderFile.truncate()
        shaderFile.write("1")
        shaderFile.close()
        render2 = ImageTk.PhotoImage(PIL.Image.open("resources/active.png"))
        shaderToggler.configure(image= render2)
        shaderToggler.image =  render2
        root.update_idletasks()
    else:
        shaderFile.close()
        shaderFile = open("settings/shader", "a")
        shaderFile.seek(0)
        shaderFile.truncate()
        shaderFile.write("0")
        shaderFile.close()
        render2 = ImageTk.PhotoImage(PIL.Image.open("resources/inactive.png"))
        shaderToggler.configure(image= render2)
        shaderToggler.image =  render2
        root.update_idletasks()
#function for toggling voice changer
def toggleVoiceChanger():
    voiceChangerFile = open("settings/voiceChanger", "r")
    if "0" in voiceChangerFile.read():
        voiceChangerFile.close()
        voiceChangerFile = open("settings/voiceChanger", "a")
        voiceChangerFile.seek(0)
        voiceChangerFile.truncate()
        voiceChangerFile.write("1")
        voiceChangerFile.close()
        render3 = ImageTk.PhotoImage(PIL.Image.open("resources/active.png"))
        voiceChangerToggler.configure(image=render3)
        voiceChangerToggler.image = render3
        root.update_idletasks()
    else:
        voiceChangerFile.close()
        voiceChangerFile = open("settings/voiceChanger", "a")
        voiceChangerFile.seek(0)
        voiceChangerFile.truncate()
        voiceChangerFile.write("0")
        voiceChangerFile.close()
        render3 = ImageTk.PhotoImage(PIL.Image.open("resources/inactive.png"))
        voiceChangerToggler.configure(image=render3)
        voiceChangerToggler.image = render3
        root.update_idletasks()
#install button - render
render = ImageTk.PhotoImage(PIL.Image.open("resources/installBTN.png"))
installBTN = Button(root, image=render, bg="#F4CD00", cursor="hand2", command=installPico, activebackground="#F4CD00")
installBTN.pack()
#shader Toggler - render2
shaderLabel = Label(root, text="Shaders:", font=("Roboto", 20), bg="#F4CD00", fg="#888787")
shaderLabel.pack()
shaderToggler = Button(root, image=render2, bg="#F4CD00", activebackground="#F4CD00", command=toggleShader, cursor="hand2")
shaderToggler.pack()
#voiceChanger Toggler - render3
voiceChangerLabel = Label(root, text="Voice Changer:", font=("Roboto", 20), bg="#F4CD00", fg="#888787")
voiceChangerLabel.pack()
voiceChangerToggler = Button(root, image=render3, bg="#F4CD00", activebackground="#F4CD00", command=toggleVoiceChanger, cursor="hand2")
voiceChangerToggler.pack()
root.mainloop()
