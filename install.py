try:
    from os import system as runcmd
    from os import listdir
    from os import mkdir
    import os
    from shutil import move
    from getpass import getuser as currentUser
    import winshell
    from win32com.client import Dispatch
    import picomc
    import os
except Exception as e:
    import ensurepip
    ensurepip.bootstrap()
    from pip._internal import main as pip
    pip(["install", "pypiwin32"])
    pip(["install", "winshell"])
    pip(["install", "picomc"])
path =  os.getcwd()
print("Installing PicoMC...")
print("Installing Forge.")
runcmd("py -m picomc version prepare 1.12.2")
runcmd("py -m picomc mod loader forge install 14.23.5.2854")
print("Installing mods, resource packs, and shaders...")
src = './picomc/'
dest = ("C:/Users/" + currentUser() + "/AppData/Roaming/.picomc/instances/default/minecraft/")
files = listdir(src)
for file in files:
    move(src + file, dest)
print("Installing LOLHUB Launcher...")
mkdir("C:/Program Files (x86)/lolhub/")
src = str(path) + '/app/'
dest = "C:/Program Files (x86)/lolhub"
move(src, dest)
desktop = winshell.desktop()
path = os.path.join(desktop, "LOLHUB Client.lnk")
target = r"C:/Program Files (x86)/lolhub/app/main.exe"
wDir = r"C:/Program Files (x86)/lolhub/app/"
icon = r"C:/Program Files (x86)/lolhub/app/resources/favicon.ico"
shell = Dispatch('WScript.Shell')
shortcut = shell.CreateShortCut(path)
shortcut.Targetpath = target
shortcut.WorkingDirectory = wDir
shortcut.IconLocation = icon
shortcut.save()
print("Done!")
